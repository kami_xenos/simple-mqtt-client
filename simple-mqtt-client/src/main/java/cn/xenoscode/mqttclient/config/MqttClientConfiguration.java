package cn.xenoscode.mqttclient.config;

import cn.xenoscode.mqttclient.mqtt.SimpleMqttClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Xenos
 * @version V1.0
 * @Package xenoscode.cn.mqttpool.config
 * @date 2020/9/1 16:44
 */

@Configuration
public class MqttClientConfiguration {
    @Value("${mqtt.host}")
    private String host;
    @Autowired
    private SimpleMqttClientProperties simpleMqttClientProperties;

    @Bean
    SimpleMqttClient mqttClient() {
        SimpleMqttClient mqttClient = new SimpleMqttClient(simpleMqttClientProperties.getClientid(),
                simpleMqttClientProperties.getUserName(),
                host,
                simpleMqttClientProperties.getTimeOut(),
                simpleMqttClientProperties.getAliveTime(),
                simpleMqttClientProperties.getTopics(),
                simpleMqttClientProperties.getQos(),
                simpleMqttClientProperties.getMaxConnectTimes());
        return mqttClient;
    }
}
