package cn.xenoscode.mqttclient.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Xenos
 * @version V1.0
 * @Package xenoscode.cn.mqttpool.config
 * @date 2020/9/4 22:39
 */
@Component
@Setter
@Getter
@ConfigurationProperties("mqtt.client")
public class SimpleMqttClientProperties {
    private String clientid;
    private String userName;
    private int timeOut;
    private int aliveTime;
    private int  maxConnectTimes;
    private String[] topics;
    private int[] qos;
}
