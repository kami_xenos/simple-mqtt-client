package cn.xenoscode.mqttclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleMqttClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleMqttClientApplication.class, args);
	}

}
