package cn.xenoscode.mqttclient.controller;

import cn.xenoscode.mqttclient.mqtt.SimpleMqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Xenos
 * @version V1.0
 * @Package xenoscode.cn.mqttpool.controller
 * @date 2020/9/4 22:59
 */
@RestController
public class TestController {
    @Autowired
    private SimpleMqttClient simpleMqttClient;

    @GetMapping("/test/publish")
    public void publishTest() {
        byte[] payload = "publish test".getBytes();
        MqttMessage mqttMessage = new MqttMessage();
        mqttMessage.setQos(2);
        mqttMessage.setPayload(payload);
        String topic = "PUBLISH_TEST";
        simpleMqttClient.publish(topic, mqttMessage);
    }
}
