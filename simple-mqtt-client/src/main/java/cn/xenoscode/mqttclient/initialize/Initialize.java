package cn.xenoscode.mqttclient.initialize;

import cn.xenoscode.mqttclient.mqtt.SimpleMqttClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @author Xenos
 * @version V1.0
 * @Package xenoscode.cn.mqttpool.initialize
 * @date 2020/9/4 21:48
 */
@Component
public class Initialize implements CommandLineRunner {
    @Autowired
    private SimpleMqttClient simpleMqttClient;

    @Override
    public void run(String... args) throws Exception {
        simpleMqttClient.connect();
    }
}
